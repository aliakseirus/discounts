import json
import requests
import os
import datetime
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

ua = UserAgent()
cur_time = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M')

headers = {
    'Accept': '*/*',
    'User-Agent': ua.random
}


def check_folder():
    '''Проверка существования директории'''
    if not os.path.exists('data'):
        os.mkdir('data')


def make_soup(url):
    '''Функция создания супа'''
    s = requests.Session()
    resp = s.get(url=url, headers=headers)
    soup = BeautifulSoup(resp.text, 'lxml')
    return soup


def collect_categories_ostrov(url):
    '''Поиск товаров со скидкой на сайте Острова Чистоты (поиск категорий, ссылок)'''
    soup = make_soup(url)

    boxes = soup.find_all('div', class_='bx_filter_parameters_box')
    for box in boxes:
        if box.get('data-property_name') == 'Тип':
            categories = box.find_all('label', class_='bx_filter_param_label')

    category_dict = {}
    for category in categories:
        cat_title = category.find('span', class_='bx_filter_param_text').text.strip()
        cat_link = category.get('for')
        category_dict[f'{cat_title}: ОЧ'] = f'{url}?set_filter=y&{cat_link}=Y'

    with open('data/ostrov_catalog.json', 'w') as file:
        json.dump(category_dict, file, indent=4, ensure_ascii=False)
    print(f'Файл data/ostrov_catalog.json создан')

    return category_dict


def collect_categories_mile(url):
    '''Поиск товаров со скидкой на сайте Mile.by (поиск категорий, ссылок)'''
    soup = make_soup(url)

    box = soup.find('div', class_='subsections')

    categories = box.find_all('a')
    category_dict = {}
    for category in categories:
        cat_title = category.text.strip()
        cat_link = category.get('href')
        category_dict[f'{cat_title}: Mile'] = f'https://mile.by{cat_link}'

    with open('data/mile_catalog.json', 'w') as file:
        json.dump(category_dict, file, indent=4, ensure_ascii=False)
    print(f'Файл data/mile_catalog.json создан')

    return category_dict


def collect_categories_oma(url):
    '''Поиск товаров со скидкой на сайте Ома (поиск категорий, ссылок)'''
    soup = make_soup(url)

    categories = soup.find_all('li', class_='js-ch-box-area')

    category_dict = {}
    for category in categories:
        cat_title = category.find('span', class_='checkbox-txt').text.strip()
        cat_link = category.find('input').get('value')
        category_dict[f'{cat_title}: Oma'] = f'{url}?WHERE={cat_link}'

    with open('data/oma_catalog.json', 'w') as file:
        json.dump(category_dict, file, indent=4, ensure_ascii=False)
    print(f'Файл data/oma_catalog.json создан')

    return category_dict


def collect_data_ostrov(cat):
    '''Поиск товаров со скидкой на сайте Острова Чистоты'''

    with open('data/ostrov_catalog.json') as file:
        catalog_dict = json.load(file)

    dest_url = catalog_dict[cat]
    soup = make_soup(dest_url)

    try:
        pagination = soup.find('div', class_='module-pagination').find_all('a', class_='dark_link')
        pages = pagination[-1].text
    except AttributeError:
        pages = 1
    print(f'{pages} страниц')

    product_list = []
    for i in range(1, int(pages) + 1):
        print(f'Смотрим страницу номер {i}')

        products = soup.find_all('div', class_='TYPE_1')
        for product in products:
            try:
                discount = product.find('div', class_='value val').text.strip()
            except AttributeError:
                continue
            title = product.find('div', class_='item-title').text.strip()
            link = product.find('a', class_='dark_link').get('href')
            full_link = f'https://ostrov-shop.by{link}'
            old_price = product.find('div', class_='old_price').text.strip()
            new_price = product.find('span', class_='price_value w').text.strip()

            product_list.append(
                {
                    'title': title,
                    'link': full_link,
                    'old_price': old_price,
                    'new_price': new_price,
                    'discount': discount
                }
            )

    check_folder()
    with open('data/ostrov_products.json', 'w') as file:
        json.dump(product_list, file, indent=4, ensure_ascii=False)

    print(f'Файл data/ostrov_products.json создан')


def collect_data_mile(cat):
    '''Поиск товаров со скидкой на сайте Mile.by'''

    with open('data/mile_catalog.json') as file:
        catalog_dict = json.load(file)

    dest_url = catalog_dict[cat]
    soup = make_soup(dest_url)

    try:
        pagination = soup.find('div', class_='pagination-wrap').find_all('a', class_='pagin-number')
        pages = pagination[-1].text
    except AttributeError:
        pages = 1
    print(f'{pages} страниц')

    product_list = []
    for i in range(1, int(pages) + 1):
        url = f'{dest_url}&PAGE={i}'
        soup = make_soup(url)
        print(f'Смотрим страницу номер {i}')

        products = soup.find_all('div', class_='anons-wrap item-')

        for product in products:
            try:
                discount = product.find('div', class_='anons-discount').text.strip()
            except AttributeError:
                continue
            title = product.find('div', class_='anons-name').text.strip()
            link = product.find('div', class_='anons-name').find('a').get('href')
            full_link = f'https://mile.by{link}'
            try:
                old_price = product.find('p', class_='price-old').text.strip()
                new_price = product.find('p', class_='price').text.strip()
            except AttributeError:
                old_price = 'Проверь сайт'
                new_price = 'Проверь сайт'

            product_list.append(
                {
                    'title': title,
                    'link': full_link,
                    'old_price': old_price,
                    'new_price': new_price,
                    'discount': discount
                }
            )

    check_folder()
    with open('data/mile_products.json', 'w') as file:
        json.dump(product_list, file, indent=4, ensure_ascii=False)

    print(f'Файл data/mile_products.json создан')


def collect_data_oma(cat):
    '''Поиск товаров со скидкой на сайте Oma'''

    with open('data/oma_catalog.json') as file:
        catalog_dict = json.load(file)

    dest_url = catalog_dict[cat]
    where = dest_url.split('?')[1]
    soup = make_soup(dest_url)

    try:
        pagination = soup.find('div', class_='page-nav_box')
        href_list = []
        hrefs = pagination.find_all('a')
        for href in hrefs:
            href_list.append(href.get('href'))
        pages = href_list[-2].split('&')[0][-1]
    except AttributeError:
        pages = 1
    print(f'{pages} страниц')

    product_list = []
    for i in range(1, int(pages) + 1):
        url = f'https://www.oma.by/sales/akcionnye-predlozheniya/?PAGEN_1={i}&{where}'
        soup = make_soup(url)
        print(f'Смотрим страницу номер {i}')

        products = soup.find_all('div', class_='product-item')

        for product in products:
            try:
                discount = product.find('div', class_='product-stickers-block').text.strip()
            except AttributeError:
                continue
            print(discount)
            title = product.find('div', class_='product-item_title-box').text.strip()
            link = product.find("a").get("href")
            full_link = f'https://www.oma.by{link}'
            try:
                old_price = product.find('span', class_='price__old').text.strip()
                new_price = product.find('span', class_='price__normal').text.strip()
            except AttributeError:
                old_price = 'Проверь сайт'
                new_price = 'Проверь сайт'

            product_list.append(
                {
                    'title': title,
                    'link': full_link,
                    'old_price': old_price,
                    'new_price': new_price,
                    'discount': discount
                }
            )

    check_folder()
    with open('data/oma_products.json', 'w') as file:
        json.dump(product_list, file, indent=4, ensure_ascii=False)

    print(f'Файл data/oma_products.json создан')


def main():
    # collect_categories_ostrov(url='https://ostrov-shop.by/promotions/skidki/')
    # collect_data_ostrov('Прокладки: ОЧ')
    # collect_categories_mile(url='https://mile.by/promotions/skidki-mesyatsa-mile-by/')
    # collect_data_mile('Посуда: Mile')
    # collect_categories_oma(url='https://www.oma.by/sales/akcionnye-predlozheniya/')
    collect_data_oma('Посуда 72: Oma')

if __name__ == '__main__':
    main()

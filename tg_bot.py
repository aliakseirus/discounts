import telebot
import json
from telebot import types
from time import sleep
from main import collect_categories_ostrov, collect_data_ostrov, collect_categories_mile, collect_data_mile, \
    collect_categories_oma, collect_data_oma

with open('bot_token') as file:
    token = file.read()

bot = telebot.TeleBot(token=token)


def print_info(way_to_file, message):
    with open(way_to_file) as file:
        data = json.load(file)
        cards = []
        for item in data:
            card = f"Товар: {item.get('title')}\n" \
                   f"{item.get('link')}\n" \
                   f"Старая цена: {item.get('old_price')}\n" \
                   f"Новая цена: {item.get('new_price')}\n" \
                   f"Размер скидки: {item.get('discount')}\n"
            bot.send_message(message, card)
            cards.append(card)
            sleep(0.5)
    bot.send_message(message, "Больше ничего нет")
    return cards


@bot.message_handler(commands='start')
def start_message(message):
    start_buttons = (
        'Остров чистоты',
        'Mile (товары месяца)',
        'Oma',
    )
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*start_buttons)
    bot.send_message(message.chat.id, 'Выберите магазин', reply_markup=keyboard)


@bot.message_handler(commands='stop')
def stop(message):
    bot.send_message(message.chat.id, 'Прекращаю работу')
    bot.close()


@bot.message_handler()
def collect_data(message):
    if message.text == '<<< Начало':
        start_message(message)

    elif message.text == 'Остров чистоты':
        bot.send_message(message.chat.id, 'Ищу информацию')
        categories = tuple(collect_categories_ostrov(url='https://ostrov-shop.by/promotions/skidki/').keys())
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add('<<< Начало', *categories)
        bot.send_message(message.chat.id, 'Выберите категорию', reply_markup=keyboard)

    elif message.text == 'Mile (товары месяца)':
        bot.send_message(message.chat.id, 'Ищу информацию')
        categories = tuple(collect_categories_mile(url='https://mile.by/promotions/skidki-mesyatsa-mile-by/').keys())
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add('<<< Начало', *categories)
        bot.send_message(message.chat.id, 'Выберите категорию', reply_markup=keyboard)

    elif message.text == 'Oma':
        bot.send_message(message.chat.id, 'Ищу информацию')
        categories = tuple(collect_categories_oma(url='https://www.oma.by/sales/akcionnye-predlozheniya/').keys())
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add('<<< Начало', *categories)
        bot.send_message(message.chat.id, 'Выберите категорию', reply_markup=keyboard)

    elif message.text:
        #  проверка соответствия введенной категории в разных магазинах
        try:
            collect_data_ostrov(message.text)
            print_info('data/ostrov_products.json', message.chat.id)
        except KeyError:
            pass

        try:
            collect_data_mile(message.text)
            print_info('data/mile_products.json', message.chat.id)
        except KeyError:
            pass

        try:
            collect_data_oma(message.text)
            print_info('data/oma_products.json', message.chat.id)
        except KeyError:
            pass

if __name__ == '__main__':
    bot.polling()
